build:
	docker build -f Dockerfile -t tiddlywiki:latest $(shell pwd)
run:
	docker run -p 8080:9876 -it tiddlywiki -v $(shell pwd)/data:/var/lib/tiddlywiki
