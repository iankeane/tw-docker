# Yo Zack

so I tried to build this using make build from the makefile here and intended to run it like:

```
docker run -p 127.0.0.1:9090:8080 -v $(pwd)/personalwiki:/home/tiddlywiki/personalwiki tiddlywiki
```

but it's not working for some reason. The intention is for the volume to hold all the tw files and
the docker to just hold all the tw dependencies and stuff. It's pretty easy to test locally,
and from everything I've seen it should be working.
