FROM node:14.8.0-alpine3.12

VOLUME /home/tiddlywiki
WORKDIR /home/tiddlywiki


RUN npm install -g tiddlywiki
RUN npm update tiddlywiki

# ADD ./personalwiki personalwiki

EXPOSE 8080

# RUN tiddlywiki personalwiki --init server
# CMD pwd && ls -a && echo "---" && pwd && ls -a /
# CMD pwd
CMD tiddlywiki personalwiki --listen
# CMD ls personalwiki
